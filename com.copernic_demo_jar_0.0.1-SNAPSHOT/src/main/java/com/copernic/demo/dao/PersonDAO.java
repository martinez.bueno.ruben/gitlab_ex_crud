/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.copernic.demo.dao;

import com.copernic.demo.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Ruben
 */
public interface PersonDAO extends JpaRepository<Person, Long>  {
    
}
